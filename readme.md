A dynamic Firewall application for Windows 2008, Windows 7, Windows 8,
Windows 10 and Windows 2016

Setup.

1) Install Visual Studio 2017 (any other should work) with C++ development
component

2) If you want to debug from VS, you have to start VS as admin.

3) Windows testsigning has to be enabled. Execute 'bcdedit /set testsigning on'
from admin CMD and restart your machine

4) Open 'Firewall.sln' and build and of the architectures or configurations.
There could be build errors related to 'msvcprtd.lib'. Find the equivalent of:
C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.16.27023\lib\onecore\$(PlatformShortName)
Put it into: Project -> Properties -> VC++ Directories -> Library directories

5) The firewall should automatically start after building

6) You can pass a file path as the first command line argument to specify the
rule list file.



Architecture.

windivert (https://reqrypt.org/windivert.html) was chosen as the interface for
sniffing packets and blocking internet access. It has a clean C interface and
works out of the box without any issues.

Initially, I tried to use Windows Filtering Platform (FWP), but I had trouble
finding code samples and couldn't get them to run. So I dropped the idea and
switched to windivert.

The application implements 3 layers: TCP packet sniffing layer (packet_sniffer.h),
TCP socket connection checker (socket_checker.h) and a layer for blocking
internet access (blocker.h).

windivert interface blocks the thread when trying to receive from a layer, so
PacketSniffer and SocketChecker are running on dedicated threads and queueing
the results.

The main application thread is responsible for loading the rules file. If rules
are successfully loaded, the main thread proceeds to a busy loop. In the loop,
it dequeues the queued events from PacketSniffer and SocketChecker. It also
processes the events, and if some of the connections have reached a limit set by
one of the rules, a message is sent to Blocker to block the host.



Problems.

The current implementation has multiple problems which are out of the scope of
this project.

Performance. Its unknown how the program would perform with thousands of rules
running over long period of time.

Reliability. Program state is stored in memory only, not saved to disk.
Re-opening the program would discard all of the accumulated network data.

Edge cases. On Firewall start, any existing socket connections will not be
accounted for. Only newly created socket connection would be tracked.

UX. Right now the firewall simply drops both inbound and outbound packets to
and from a host. This means that when user tries to connect to a host, he does
not get a response. Instead, it seems as if the connection hangs. One way to
improve this would be to send a TCP Reset packet to notify the client or the
host that the connection is closing.
