#include "firewall/packet_sniffer.h"

#include "firewall/basic.h"
#include "firewall/format.h"
#include "firewall/util.h"
#include "firewall/windivert_util.h"

namespace firewall {

PacketSniffer::~PacketSniffer() {}

void PacketSniffer::StartLoop(const Rules& rules) {
  std::string filter = rules.FormatRulesFilter(false /* limit_seconds */,
                                               true /* limit_bytes */);
  if (filter.empty()) {
    return;  // There are no rules limiting by size
  }
  filter = "inbound and (" + filter + ")";

  layer_ =
      WinDivertOpen(filter.c_str(), WINDIVERT_LAYER_NETWORK, 0 /* priority */,
                    WINDIVERT_FLAG_SNIFF | WINDIVERT_FLAG_RECV_ONLY);
  bool status = CheckLayer(layer_);
  assert(status);

  thread_ = std::make_unique<std::thread>(&PacketSniffer::RunLoop, this);
}

void PacketSniffer::RunLoop() {
  while (true) {
    char packet[WINDIVERT_MTU_MAX];
    uint32_t packet_length = 0;
    WINDIVERT_ADDRESS address;
    if (!WinDivertRecv(layer_, packet, sizeof(packet), &packet_length,
                       &address)) {
      fprintf(stderr, "warning: failed to read packet (%d)\n", GetLastError());
      continue;
    }

    PWINDIVERT_IPHDR ip_header;
    if (!WinDivertHelperParsePacket(packet, packet_length, &ip_header, NULL,
                                    NULL, NULL, NULL, NULL, NULL, NULL, NULL,
                                    NULL, NULL)) {
      fprintf(stderr, "warning: failed to parse packet (%d)\n", GetLastError());
      continue;
    }

    std::lock_guard<std::mutex> lock(mutex_);
    packet_queue_.push_back(Packet{ntohl(ip_header->SrcAddr), packet_length});
  }
}

std::vector<PacketSniffer::Packet> PacketSniffer::PopPackets() {
  std::lock_guard<std::mutex> lock(mutex_);
  return std::move(packet_queue_);
}

}  // namespace firewall
