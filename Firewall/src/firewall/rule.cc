#include "firewall/rule.h"

#include "firewall/format.h"
#include "firewall/util.h"

namespace firewall {

namespace {

// '80.249.99.148/32'
bool ParseAddress(const std::string& full_address,
                  Rule* rule,
                  std::string* error_message) {
  std::vector<std::string> pair = util::SplitString(full_address, "/");
  if (pair.size() != 2) {
    *error_message = "missing slash notation";
    return false;
  }

  uint32_t ip = 0;
  if (!WinDivertHelperParseIPv4Address(pair.at(0).c_str(), &ip)) {
    *error_message = "invalid IP address";
    return false;
  }

  int subnet = -1;
  try {
    subnet = std::stoi(pair.at(1));
  } catch (...) {
  }

  if (subnet < 0 || subnet > 32) {
    *error_message = "invalid slash notation, should be 0-32";
    return false;
  }

  uint32_t mask = util::GetCidrMask(subnet);
  rule->ip_min = ip & mask;
  rule->ip_max = rule->ip_min | (0xFFFFFFFF ^ mask);
  // TODO: is it OK to include Network and Broadcast addresses?

  return true;
}

// '11mb'
bool ParseLimit(const std::string& limit_string,
                Rule* rule,
                std::string* error_message) {
  int64_t limit = -1;
  std::size_t unit_position = 0;
  try {
    limit = std::stol(limit_string, &unit_position);
  } catch (...) {
  }

  if (limit < 0) {
    *error_message = "limit has to be a positive 64bit integer";
    return false;
  }

  std::string unit =
      limit_string.substr(unit_position, limit_string.length() - unit_position);
  util::trim(unit);
  if (unit == "kb") {
    rule->limit_bytes = limit * 1024;
  } else if (unit == "mb") {
    rule->limit_bytes = limit * 1024 * 1024;
  } else if (unit == "gb") {
    rule->limit_bytes = limit * 1024 * 1024 * 1024;
  } else if (unit == "s") {
    rule->limit_seconds = limit;
  } else if (unit == "m") {
    rule->limit_seconds = limit * 60;
  } else if (unit == "h") {
    rule->limit_seconds = limit * 60 * 60;
  } else {
    *error_message = "invalid limit unit";
    return false;
  }

  return true;
}

void PrintError(const std::string& message, const std::string& source_line) {
  std::cerr << "error: " << message << " - \"" << source_line << "\"";
}

// '80.249.99.148/32 11mb'
bool ParseRule(const std::string& line, Rule* rule) {
  rule->source = line;

  std::vector<std::string> pair = util::SplitString(line, " ");
  if (pair.size() != 2) {
    PrintError("missing limit", line);
    return false;
  }

  std::string error_message;
  if (!ParseAddress(pair.at(0), rule, &error_message)) {
    PrintError(error_message, line);
    return false;
  }

  if (!ParseLimit(pair.at(1), rule, &error_message)) {
    PrintError(error_message, line);
    return false;
  }

  return true;
}

}  // namespace

bool Rules::TryParseRule(const std::string& line) {
  Rule rule;
  if (ParseRule(line, &rule)) {
    rules_.push_back(rule);
    return true;
  }
  return false;
}

const Rule* Rules::FindBrokenTimeRule(uint32_t ip, int64_t limit) const {
  for (const auto& rule : rules_) {
    if (rule.IsIpInRange(ip) && rule.CheckTimeLimit(limit)) {
      return &rule;
    }
  }
  return nullptr;
}

const Rule* Rules::FindBrokenSizeRule(uint32_t ip, int64_t limit) const {
  for (const auto& rule : rules_) {
    if (rule.IsIpInRange(ip) && rule.CheckSizeLimit(limit)) {
      return &rule;
    }
  }
  return nullptr;
}

std::string Rules::FormatRulesFilter(bool limit_seconds,
                                     bool limit_bytes) const {
  std::vector<std::string> filters;
  for (const auto& rule : rules_) {
    if (limit_seconds && rule.limit_seconds ||
        limit_bytes && rule.limit_bytes) {
      if (rule.ip_min != rule.ip_max) {
        filters.push_back(FORMAT("(remoteAddr >= @ and remoteAddr <= @)",
                                 util::FormatIP(rule.ip_min),
                                 util::FormatIP(rule.ip_max)));
      } else {
        filters.push_back(
            FORMAT("remoteAddr == @", util::FormatIP(rule.ip_min)));
      }
    }
  }
  return util::JoinString(filters, " or ");
}

}  // namespace firewall
