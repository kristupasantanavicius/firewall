#include "firewall/firewall.h"

#include "firewall/format.h"
#include "firewall/rule.h"
#include "firewall/util.h"

namespace firewall {

void Firewall::StartFirewall(std::unique_ptr<Rules> rules) {
  rules_ = std::move(rules);

  sniffer_ = std::make_unique<PacketSniffer>();
  sniffer_->StartLoop(*rules_);

  socket_checker_ = std::make_unique<SocketChecker>();
  socket_checker_->StartLoop(*rules_);

  blocker_ = std::make_unique<Blocker>();

  RunLoop();
}

void Firewall::RunLoop() {
  int64_t last_live_socket_update = 0;

  while (true) {
    UpdatePacketSniffer();

    UpdateSocketChecker();

    if (util::GetTime() - last_live_socket_update >= 1000) {
      last_live_socket_update = util::GetTime();
      UpdateLiveSockets();
    }
  }
}

void Firewall::UpdatePacketSniffer() {
  for (auto& packet : sniffer_->PopPackets()) {
    HostInfo& info = host_data_[packet.ip];
    info.size += packet.length;
    if (const Rule* rule = rules_->FindBrokenSizeRule(packet.ip, info.size)) {
      blocker_->BlockIP(packet.ip);
      DisconnectSocket(packet.ip);
      std::cerr << FORMAT("Host @ blocked due to size limit by '@'",
                          util::FormatIP(packet.ip), rule->source)
                << std::endl;
    }
  }
}

void Firewall::UpdateSocketChecker() {
  for (auto& socket : socket_checker_->PopSocketStateChanges()) {
    HostInfo& info = host_data_[socket.ip];
    if (socket.connected) {
      ConnectSocket(socket.ip);
    } else {
      DisconnectSocket(socket.ip);
    }
  }
}

void Firewall::UpdateLiveSockets() {
  // Can't modify |live_sockets_| while iterating
  std::vector<uint32_t> remove_sockets_;

  for (auto& iter : live_sockets_) {
    uint32_t ip = iter.first;
    int64_t connected_timestamp = iter.second;

    int64_t connection_time = (util::GetTime() - connected_timestamp);
    int64_t time_millis = host_data_[ip].time_millis + connection_time;

    if (const Rule* rule = rules_->FindBrokenTimeRule(ip, time_millis / 1000)) {
      remove_sockets_.push_back(ip);
    }
  }

  for (auto& ip : remove_sockets_) {
    DisconnectSocket(ip);
  }
}

void Firewall::ConnectSocket(uint32_t ip) {
  live_sockets_[ip] = util::GetTime();
}

void Firewall::DisconnectSocket(uint32_t ip) {
  auto iter = live_sockets_.find(ip);
  if (iter == live_sockets_.end()) {
    return;
  }
  host_data_[ip].time_millis += (util::GetTime() - iter->second);
  live_sockets_.erase(iter);

  if (const Rule* rule =
          rules_->FindBrokenTimeRule(ip, host_data_[ip].time_millis / 1000)) {
    blocker_->BlockIP(ip);
    std::cerr << FORMAT("Host @ blocked due to time limit by '@'",
                        util::FormatIP(ip), rule->source)
              << std::endl;
  }
}

}  // namespace firewall
