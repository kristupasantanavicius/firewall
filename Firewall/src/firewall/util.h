#pragma once

#include "windivert.h"

#include <stdint.h>
#include <algorithm>
#include <cctype>
#include <iostream>
#include <locale>
#include <string>
#include <vector>

namespace util {

inline uint32_t ntohl(uint32_t x) {
  return WinDivertHelperNtohl(x);
}

inline uint32_t htonl(uint32_t x) {
  return WinDivertHelperHtonl(x);
}

inline void ltrim(std::string& s) {
  s.erase(s.begin(), std::find_if(s.begin(), s.end(),
                                  [](int ch) { return !std::isspace(ch); }));
}

inline void rtrim(std::string& s) {
  s.erase(std::find_if(s.rbegin(), s.rend(),
                       [](int ch) { return !std::isspace(ch); })
              .base(),
          s.end());
}

inline void trim(std::string& s) {
  ltrim(s);
  rtrim(s);
}

std::string FormatIP(uint32_t address);

uint32_t ParseIP(const std::string& address);

uint32_t GetCidrMask(int mask);

std::vector<std::string> SplitString(const std::string& value,
                                     const std::string& delimeter);

std::string JoinString(const std::vector<std::string>& array,
                       const std::string& separator);

int64_t GetTime();

}  // namespace util
