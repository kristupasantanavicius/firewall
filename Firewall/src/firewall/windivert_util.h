#pragma once

#include "stdio.h"
#include "windivert.h"

namespace firewall {

bool CheckLayer(HANDLE layer);

}  // namespace firewall
