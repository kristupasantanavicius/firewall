#pragma once

#include "firewall/rule.h"

#include <memory>
#include <mutex>
#include <thread>
#include <vector>
#include "windivert.h"

namespace firewall {

class SocketChecker {
 public:
  struct SocketState {
    uint32_t ip;
    bool connected;
  };

  ~SocketChecker();

  void StartLoop(const Rules& rules);

  std::vector<SocketState> PopSocketStateChanges();

 private:
  void RunLoop();

  std::unique_ptr<std::thread> thread_;

  HANDLE layer_;

  std::mutex mutex_;

  std::vector<SocketState> state_queue_;
};

}  // namespace firewall
