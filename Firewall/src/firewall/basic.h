#pragma once

#define crash(what)                                                            \
  do {                                                                         \
    printf("-------------------------------------------------------------\n"); \
    printf("Assertion failed: (%s)\n", #what);                                 \
    printf("At: %s:%d\n", __FILE__, __LINE__);                                 \
    printf("-------------------------------------------------------------\n"); \
    (*(volatile int*)nullptr) = 0;                                             \
    throw std::runtime_error(                                                  \
        "crash() exception to suppress 'no return statement' compiler "        \
        "warnings");                                                           \
  } while (0)

#define assert(cond) \
  do {               \
    if (!(cond))     \
      crash(cond);   \
  } while (0)

#define UNUSED(x) static_cast<void>(x)
