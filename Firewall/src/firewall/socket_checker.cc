#include "firewall/socket_checker.h"

#include "firewall/basic.h"
#include "firewall/format.h"
#include "firewall/util.h"
#include "firewall/windivert_util.h"

namespace firewall {

SocketChecker::~SocketChecker() {}

void SocketChecker::StartLoop(const Rules& rules) {
  std::string filter = rules.FormatRulesFilter(true /* limit_seconds */,
                                               false /* limit_bytes */);

  if (filter.empty()) {
    return;  // There are no rules limiting connection time
  }

  layer_ =
      WinDivertOpen(filter.c_str(), WINDIVERT_LAYER_SOCKET, 0 /* priority */,
                    WINDIVERT_FLAG_SNIFF | WINDIVERT_FLAG_RECV_ONLY);
  bool status = CheckLayer(layer_);
  assert(status);

  thread_ = std::make_unique<std::thread>(&SocketChecker::RunLoop, this);
}

void SocketChecker::RunLoop() {
  while (true) {
    WINDIVERT_ADDRESS address;
    if (!WinDivertRecv(layer_, nullptr, 0, nullptr, &address)) {
      fprintf(stderr, "warning: failed to read packet (%d)\n", GetLastError());
      continue;
    }

    if (address.Event == WINDIVERT_EVENT_SOCKET_CONNECT ||
        address.Event == WINDIVERT_EVENT_SOCKET_CLOSE) {
      std::lock_guard<std::mutex> lock(mutex_);
      state_queue_.push_back(
          SocketState{*address.Socket.RemoteAddr,
                      address.Event == WINDIVERT_EVENT_SOCKET_CONNECT});
    }
  }
}

std::vector<SocketChecker::SocketState> SocketChecker::PopSocketStateChanges() {
  std::lock_guard<std::mutex> lock(mutex_);
  return std::move(state_queue_);
}

}  // namespace firewall
