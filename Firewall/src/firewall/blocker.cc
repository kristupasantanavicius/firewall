#include "firewall/blocker.h"

#include "firewall/basic.h"
#include "firewall/format.h"
#include "firewall/util.h"
#include "firewall/windivert_util.h"

#include <iostream>

namespace firewall {

void Blocker::BlockIP(uint32_t ip) {
  auto iter = blocked_ips_.find(ip);
  if (iter != blocked_ips_.end()) {
    std::cerr << "warning: trying to block same IP second time" << std::endl;
    return;
  }
  blocked_ips_.insert(ip);

  std::vector<std::string> filters;
  for (auto& ip : blocked_ips_) {
    filters.push_back(FORMAT("remoteAddr == @", util::FormatIP(ip)));
  }
  std::string filter = util::JoinString(filters, " or ");

  HANDLE network_layer = WinDivertOpen(filter.c_str(), WINDIVERT_LAYER_NETWORK,
                                       0 /* priority */, WINDIVERT_FLAG_DROP);
  bool status = CheckLayer(network_layer);
  assert(status);

  HANDLE socket_layer =
      WinDivertOpen(filter.c_str(), WINDIVERT_LAYER_SOCKET, 0 /* priority */,
                    WINDIVERT_FLAG_RECV_ONLY);
  status = CheckLayer(socket_layer);
  assert(status);

  if (network_layer_ != INVALID_HANDLE_VALUE) {
    status = WinDivertClose(network_layer_);
  }

  if (socket_layer_ != INVALID_HANDLE_VALUE) {
    status = WinDivertClose(socket_layer_);
  }

  network_layer_ = network_layer;
  socket_layer_ = socket_layer;
}

}  // namespace firewall
