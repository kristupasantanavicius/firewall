#include "firewall/format.h"
#include "firewall/basic.h"

std::string stringFormatPrivateImpl(const std::string& format,
                                    const std::string* args,
                                    std::size_t argCount) {
  std::string output;
  std::size_t argIndex = 0;
  bool escape = false;

  for (int i = 0; i < (int)format.size(); i++) {
    if (escape) {
      escape = false;
      output += format[i];
    } else {
      if (format[i] == '\\') {
        escape = true;
      } else if (format[i] == '@') {
        if (argIndex >= argCount) {
          assert(false && "Not enough arguments!");
          output += "[ERROR]";
        } else {
          output += args[argIndex];
          argIndex++;
        }
      } else {
        output += format[i];
      }
    }
  }
  return output;
}
