#pragma once

#include <string>

// Don't use this directly!!!!!!!
extern std::string stringFormatPrivateImpl(const std::string& format,
                                           const std::string* args,
                                           std::size_t argCount);

inline std::string make_string(const std::string& str) {
  return str;
}

inline std::string make_string(int number) {
  return std::to_string(number);
}

//
// This is a lot of copy paste..
// If you want to add more arguments you have to add it here.
inline std::string FORMAT(const std::string& format) {
  return stringFormatPrivateImpl(format, nullptr, 0);
}

template <typename ARG0>
inline std::string FORMAT(const std::string& format, const ARG0& arg0) {
  std::string list[] = {make_string(arg0)};
  return stringFormatPrivateImpl(format, list, 1);
}

template <typename ARG0, typename ARG1>
inline std::string FORMAT(const std::string& format,
                          const ARG0& arg0,
                          const ARG1& arg1) {
  std::string list[] = {make_string(arg0), make_string(arg1)};
  return stringFormatPrivateImpl(format, list, 2);
}

template <typename ARG0, typename ARG1, typename ARG2>
inline std::string FORMAT(const std::string& format,
                          const ARG0& arg0,
                          const ARG1& arg1,
                          const ARG2& arg2) {
  std::string list[] = {make_string(arg0), make_string(arg1),
                        make_string(arg2)};
  return stringFormatPrivateImpl(format, list, 3);
}

template <typename ARG0, typename ARG1, typename ARG2, typename ARG3>
inline std::string FORMAT(const std::string& format,
                          const ARG0& arg0,
                          const ARG1& arg1,
                          const ARG2& arg2,
                          const ARG3& arg3) {
  std::string list[] = {make_string(arg0), make_string(arg1), make_string(arg2),
                        make_string(arg3)};
  return stringFormatPrivateImpl(format, list, 4);
}

template <typename ARG0,
          typename ARG1,
          typename ARG2,
          typename ARG3,
          typename ARG4>
inline std::string FORMAT(const std::string& format,
                          const ARG0& arg0,
                          const ARG1& arg1,
                          const ARG2& arg2,
                          const ARG3& arg3,
                          const ARG4& arg4) {
  std::string list[] = {make_string(arg0), make_string(arg1), make_string(arg2),
                        make_string(arg3), make_string(arg4)};
  return stringFormatPrivateImpl(format, list, 5);
}
