#pragma once

#include <unordered_set>
#include "windivert.h"

namespace firewall {

class Blocker {
 public:
  void BlockIP(uint32_t ip);

 private:
  HANDLE network_layer_ = INVALID_HANDLE_VALUE;

  HANDLE socket_layer_ = INVALID_HANDLE_VALUE;

  std::unordered_set<uint32_t> blocked_ips_;
};

}  // namespace firewall
