#pragma once

#include <stdint.h>
#include <string>
#include <vector>

namespace firewall {

struct Rule {
  int64_t limit_seconds = 0;
  int64_t limit_bytes = 0;
  uint32_t ip_min = 0;
  uint32_t ip_max = 0;
  std::string source;  // String from rules file, for console output

  bool IsIpInRange(uint32_t ip) const { return ip >= ip_min && ip <= ip_max; }

  bool CheckTimeLimit(int64_t size) const {
    return limit_seconds && size >= limit_seconds;
  }

  bool CheckSizeLimit(int64_t size) const {
    return limit_bytes && size >= limit_bytes;
  }
};

class Rules {
 public:
  bool TryParseRule(const std::string& line);

  const Rule* FindBrokenTimeRule(uint32_t ip, int64_t limit) const;

  const Rule* FindBrokenSizeRule(uint32_t ip, int64_t limit) const;

  std::string FormatRulesFilter(bool limit_seconds, bool limit_bytes) const;

 private:
  std::vector<Rule> rules_;
};

}  // namespace firewall
