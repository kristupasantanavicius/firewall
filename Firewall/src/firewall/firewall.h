#pragma once

#include "firewall/blocker.h"
#include "firewall/packet_sniffer.h"
#include "firewall/rule.h"
#include "firewall/socket_checker.h"

#include <memory>
#include <unordered_map>

namespace firewall {

class Firewall {
  struct HostInfo {
    int64_t size = 0;
    int64_t time_millis = 0;
  };

 public:
  void StartFirewall(std::unique_ptr<Rules> rules);

 private:
  void RunLoop();

  void UpdatePacketSniffer();

  void UpdateSocketChecker();

  void UpdateLiveSockets();

  void ConnectSocket(uint32_t ip);

  void DisconnectSocket(uint32_t ip);

  void CheckSocketRule(uint32_t ip);

  std::unique_ptr<Rules> rules_;

  std::unique_ptr<PacketSniffer> sniffer_;
  std::unique_ptr<SocketChecker> socket_checker_;
  std::unique_ptr<Blocker> blocker_;

  std::unordered_map<uint32_t, HostInfo> host_data_;

  std::unordered_map<uint32_t, int64_t> live_sockets_;
};

}  // namespace firewall
