#include "firewall/windivert_util.h"

namespace firewall {

bool CheckLayer(HANDLE layer) {
  if (layer != INVALID_HANDLE_VALUE) {
    return true;
  }
  if (GetLastError() == ERROR_INVALID_PARAMETER) {
    fprintf(
        stderr,
        "error: failed to open the WinDivert device, bad filter string (%d)\n",
        GetLastError());
    return false;
  }
  fprintf(stderr, "error: failed to open the WinDivert device (%d)\n",
          GetLastError());
  return false;
}

}  // namespace firewall
