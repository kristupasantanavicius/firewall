#include "firewall/util.h"

#include "firewall/basic.h"

#include <chrono>

namespace util {

std::string FormatIP(uint32_t address) {
  char buffer[16];
  bool status =
      WinDivertHelperFormatIPv4Address(address, buffer, sizeof(buffer));
  assert(status);
  return buffer;
}

std::vector<std::string> SplitString(const std::string& value,
                                     const std::string& delimeter) {
  std::vector<std::string> result;
  std::size_t position = 0;
  while (true) {
    std::size_t found = value.find(delimeter, position);
    if (found == std::string::npos) {
      break;
    }
    std::string token = value.substr(position, found - position);
    trim(token);
    result.push_back(std::move(token));
    position = found + delimeter.size();
  }
  if (position < value.length()) {
    std::string token = value.substr(position, value.length() - position);
    trim(token);
    result.push_back(std::move(token));
  }
  return result;
}

std::string JoinString(const std::vector<std::string>& array,
                       const std::string& separator) {
  if (array.empty()) {
    return "";
  }

  std::string result;
  for (auto iter = array.begin(); iter < array.end() - 1; iter++) {
    result += *iter;
    result += separator;
  }
  result += array.back();

  return result;
}

uint32_t GetCidrMask(int mask) {
  static uint32_t table[33] = {
      /* 0 */ 0x00000000,  /* 1 */ 0x80000000,  /* 2 */ 0xC0000000,
      /* 3 */ 0xE0000000,  /* 4 */ 0xF0000000,  /* 5 */ 0xF8000000,
      /* 6 */ 0xFC000000,  /* 7 */ 0xFE000000,  /* 8 */ 0xFF000000,
      /* 9 */ 0xFF800000,  /* 10 */ 0xFFC00000, /* 11 */ 0xFFE00000,
      /* 12 */ 0xFFF00000, /* 13 */ 0xFFF80000, /* 14 */ 0xFFFC0000,
      /* 15 */ 0xFFFE0000, /* 16 */ 0xFFFF0000, /* 17 */ 0xFFFF8000,
      /* 18 */ 0xFFFFC000, /* 19 */ 0xFFFFE000, /* 20 */ 0xFFFFF000,
      /* 21 */ 0xFFFFF800, /* 22 */ 0xFFFFFC00, /* 23 */ 0xFFFFFE00,
      /* 24 */ 0xFFFFFF00, /* 25 */ 0xFFFFFF80, /* 26 */ 0xFFFFFFC0,
      /* 27 */ 0xFFFFFFE0, /* 28 */ 0xFFFFFFF0, /* 29 */ 0xFFFFFFF8,
      /* 30 */ 0xFFFFFFFC, /* 31 */ 0xFFFFFFFE, /* 32 */ 0xFFFFFFFF,
  };

  if (mask >= 0 && mask < sizeof(table)) {
    return table[mask];
  } else {
    return 0xFFFFFFFF;
  }
}

int64_t GetTime() {
  using namespace std::chrono;
  return duration_cast<milliseconds>(system_clock::now().time_since_epoch())
      .count();
}

}  // namespace util
