#pragma once

#include "firewall/rule.h"

#include <memory>
#include <mutex>
#include <thread>
#include <vector>
#include "windivert.h"

namespace firewall {

class PacketSniffer {
 public:
  struct Packet {
    uint32_t ip;
    uint32_t length;
  };

  ~PacketSniffer();

  void StartLoop(const Rules& rules);

  std::vector<Packet> PopPackets();

 private:
  void RunLoop();

  std::unique_ptr<std::thread> thread_;

  HANDLE layer_;

  std::mutex mutex_;

  std::vector<Packet> packet_queue_;

};

}  // namespace firewall
