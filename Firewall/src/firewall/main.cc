#include "firewall/firewall.h"
#include "firewall/rule.h"
#include "firewall/util.h"

#include <fstream>
#include <iostream>

namespace firewall {

bool ParseRules(const std::string& file_path, Rules* rules) {
  std::ifstream file_stream(file_path);
  if (!file_stream.is_open()) {
    std::cerr << "couldn't open rule file: " << file_path << std::endl;
    return false;
  }

  std::string line;
  while (std::getline(file_stream, line)) {
    util::trim(line);
    if (line.empty()) {
      continue;
    }
    if (!rules->TryParseRule(line)) {
      return false;
    }
  }

  return true;
}

}  // namespace firewall

int main(int argc, char** argv) {
  if (argc < 2) {
    std::cerr << "error: 1st parameter is a rule file path" << std::endl;
    return 1;
  }

  std::unique_ptr<firewall::Rules> rules = std::make_unique<firewall::Rules>();
  if (!ParseRules(argv[1], rules.get())) {
    return 1;
  }

  firewall::Firewall firewall;
  firewall.StartFirewall(std::move(rules));

  return 0;
}
